#include "bmp.h"
#include <stdbool.h>

static const uint32_t bfType = 0x4D42;
static const uint32_t biSize = 40;
static const size_t biBitCount = 24;

struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

bool check_header(struct bmp_header* header, FILE* in) {
    if ((fread(header, sizeof(struct bmp_header), 1, in) != 1) || (header->bfType != 0x4D42)) {
        return false;
    }
    return true;
}

static size_t bmp_padding(uint64_t width)
{
    return (4 - (width * 3) % 4);
}

static size_t put_padding(struct image *img, FILE *out)
{
    uint8_t paddings[4] = {0};
    uint8_t padding = bmp_padding(img->width);
    return fwrite(paddings, padding, 1, out);
}

static uint32_t bmp_size(struct image *img)
{
    uint64_t w = img->width;
    uint64_t h = img->height;
    return (sizeof(struct pixel) * w * h + bmp_padding(w)) * h;
}

static struct bmp_header make_bmp_header(struct image *img)
{
    struct bmp_header header;
    header.bfType = bfType;
    header.bfileSize = sizeof(struct bmp_header) + bmp_size(img);
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = biSize;
    header.biHeight = img->height;
    header.biWidth = img->width;
    header.biPlanes = 1;
    header.biBitCount = biBitCount;
    header.biCompression = 0;
    header.biSizeImage = bmp_size(img);
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;
    return header;
}

enum read_status from_bmp(FILE *in, struct image *img)
{
    struct bmp_header header = {0};
    if (!check_header(&header, in)) {
        return READ_INVALID_HEADER;
    }
    if (header.biBitCount != biBitCount)
    {
        return READ_INVALID_BITS;
    }

    struct image newp = make_img(header.biWidth, header.biHeight);
    uint64_t w = newp.width;
    uint64_t h = newp.height;
    for (size_t i = 0; i < h; i++)
    {
        if (fread(newp.data + i * w, img_size_row(&newp), 1, in) != 1)
        {
            img_free(newp);
            return READ_ROW_FAILED;
        }
        if (fseek(in, (long) bmp_padding(w), SEEK_CUR))
        {
            img_free(newp);
            return READ_PADDING_FAILED;
        }
    }

    *img = newp;
    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image *img)
{
    uint64_t w = img->width;
    uint64_t h = img->height;
    struct bmp_header header = make_bmp_header(img);

    if (img->data != NULL)
    {
        if (fwrite(&header, sizeof(struct bmp_header), 1, out) < 1)
        {
            return WRITE_ERROR;
        }

        if (fseek(out, header.bOffBits, SEEK_SET))
        {
            return SEEK_ERROR;
        };

        for (size_t i = 0; i < h; i++)
        {
            if (fwrite(img->data + i * w, img_size_row(img), 1, out) != 1)
            {
                return WRITE_ERROR;
            };
            if (put_padding(img, out) != 1) 
            {
                return PDD_ERROR;
            }
        }
    }
    return WRITE_OK;
}
