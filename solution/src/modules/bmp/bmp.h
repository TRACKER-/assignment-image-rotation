#ifndef BMP_H
#define BMP_H
#include "../../enums/read/read.h"
#include "../../enums/write/write.h"
#include "../image/image.h"
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

enum read_status from_bmp(FILE *in, struct image *img);
enum write_status to_bmp(FILE *out, struct image *img);

#endif
