#include "image.h"

struct image make_img(uint64_t width, uint64_t height)
{
    struct image img = {width, height, NULL};
    img.data = malloc(width * height * sizeof(struct pixel));
    return img;
}

void img_free(struct image img)
{
    free(img.data);
}

uint32_t img_size_row(struct image const *img)
{
    return (sizeof(struct pixel) * img->width);
}

size_t new_address(const struct image img, size_t row, size_t col) 
{
    return (col * img.height + (img.height - row - 1));
}

size_t old_address(const struct image img, size_t row, size_t col) 
{
    return (row * img.width + col);
}
