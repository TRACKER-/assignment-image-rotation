#ifndef IMAGE_H
#define IMAGE_H
#include <stdint.h>
#include <stdlib.h>

struct pixel
{
    uint8_t b, g, r;
};

struct image
{
    uint64_t width, height;
    struct pixel* data;
};

struct image make_img(uint64_t width, uint64_t height);
void img_free(struct image img);
uint32_t img_size_row(struct image const *img);
size_t new_address(const struct image img, size_t row, size_t col);
size_t old_address(const struct image img, size_t row, size_t col);

#endif
