#include "file.h"

FILE *open_file(const char *path, const char *mode)
{
    FILE *file = fopen(path, mode);
    return file;
}

void close_file(FILE *file)
{
    fclose(file);
}
