#ifndef FILE_H
#define FILE_H
#include <stdio.h>

FILE *open_file(const char *path, const char *mode);
void close_file(FILE *file);

#endif
