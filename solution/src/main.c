#include "modules/bmp/bmp.h"
#include "modules/file/file.h"
#include "rotate/rotate.h"

int main(int argc, char **argv)
{
    if (argc < 3)
    {
        fprintf(stderr, "Need more arguments");
        return NEED_MORE_ARGS;        
    }

    FILE *f_in = open_file(argv[1], "rb");
    FILE *f_out = open_file(argv[2], "wb");
    if (f_out == NULL)
    {
        if (f_in == NULL)
        {
            fprintf(stderr, "Files NO\n");
            return BOTH_NO;
        }
        else
        {
            fprintf(stderr, "Out NO\n");
            return OUT_NO;
        }
    }
    else
    {
        if (f_in == NULL)
        {
            fprintf(stderr, "In NO\n");
            return IN_NO;
        }
    }

    struct image img = {0, 0, NULL};
    enum read_status r_res = from_bmp(f_in, &img);
    if (r_res != READ_OK)
    {
        fprintf(stderr, "FAIL with reading");
        return r_res;
    }

    struct image rotated_img = rotate(img);
    enum write_status w_res = to_bmp(f_out, &rotated_img);
    if (w_res != WRITE_OK)
    {
        fprintf(stderr, "FAIL with writing");
        return w_res;
    }

    img_free(rotated_img);
    img_free(img);
    
    close_file(f_out);
    close_file(f_in);

    return 0;
}
