#include "rotate.h"

struct image rotate(const struct image img)
{
	uint64_t w = img.width;
	uint64_t h = img.height;
	struct image rotated_img = make_img(img.height, img.width);
	for (size_t row = 0; row < h; row++)
	{
		for (size_t col = 0; col < w; col++)
		{
			rotated_img.data[new_address(img, row, col)] = img.data[old_address(img, row, col)];
		}
	}
	return rotated_img;
}
