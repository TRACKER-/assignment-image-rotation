#ifndef ROTATE_H
#define ROTATE_H
#include "../modules/image/image.h"
#include <stddef.h>

struct image rotate(const struct image img);

#endif
