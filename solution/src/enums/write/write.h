#ifndef WRITE_H
#define WRITE_H

enum write_status
{
    WRITE_OK = 0,
    WRITE_ERROR,
    HEADER_ERROR,
    SEEK_ERROR,
    PDD_ERROR
};

#endif
